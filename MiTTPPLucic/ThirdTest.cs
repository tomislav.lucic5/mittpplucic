﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;

namespace MiTTPPLucic;

[Parallelizable(ParallelScope.Self)]
[TestFixture]
public class ThirdTest : PageTest
{
    [Test]
    public async Task PerformanceMeter()
    {
        var browser = await Playwright.Chromium.LaunchAsync();
        var context = await browser.NewContextAsync();
        await context.SetGeolocationAsync(new() { Latitude = -30, Longitude = 30 });
        var page = await context.NewPageAsync();
        await page.ScreenshotAsync();

        await page.EvaluateAsync("window.performance.measure('Start');");

        await page.GotoAsync("https://www.phptravels.net/");

        var metrics = await page.EvaluateAsync<dynamic>(@"() => {
            window.performance.measure('End');
            const measure = window.performance.getEntriesByType('measure')[0];
            return {
                startTime: measure.startTime,
                endTime: measure.duration,
                metrics: JSON.stringify(performance.timing)
            };
        }");

        Console.WriteLine($"Vrijeme potrebno za pokretanje: {metrics.startTime}");
        Console.WriteLine($"Trajanje: {metrics.endTime/1000} sekundi");
        Console.WriteLine($"Mjerenja performansi: {metrics.metrics}");

        await browser.CloseAsync();
    }
}