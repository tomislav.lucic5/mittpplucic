﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;

namespace MiTTPPLucic;

[Parallelizable(ParallelScope.Self)]
[TestFixture]
public class FirstTest : PageTest
{
    [Test]
    public async Task CompleteWebForm()
    {
        await Page.GotoAsync("https://formy-project.herokuapp.com/");

        var completeWebFormElement = Page.GetByRole(AriaRole.Link, new() { Name = "Complete Web Form" });
        await completeWebFormElement.ClickAsync();

        await Page.GetByPlaceholder("Enter first name").FillAsync("Tomislav");

        await Task.Delay(1000);

        await Page.GetByPlaceholder("Enter last name").FillAsync("Lucic");

        await Task.Delay(1000);

        await Page.GetByPlaceholder("Enter your job title").FillAsync("Backend developer");

        await Task.Delay(1000);

        await Page.Locator("xpath=//*[@id=\"radio-button-2\"]").ClickAsync();

        await Task.Delay(1000);

        await Page.Locator("xpath=//*[@id=\"checkbox-1\"]").ClickAsync();

        await Task.Delay(1000);

        await Page.GetByLabel("Years of experience:").SelectOptionAsync(new[] { "0-1" });

        await Task.Delay(1000);

        await Page.GetByPlaceholder("mm/dd/yyyy").FillAsync("12/06/2001");

        await Task.Delay(1000);

        var submitButton = Page.GetByRole(AriaRole.Button, new() { Name = "Submit" });
        await submitButton.ClickAsync();

        await Task.Delay(1000);

        await Expect(Page.GetByText("Thanks for submitting your form")).ToBeVisibleAsync();

        await Task.Delay(1000);
    }
}