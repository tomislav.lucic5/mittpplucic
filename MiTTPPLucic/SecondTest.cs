﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;

namespace MiTTPPLucic;

[Parallelizable(ParallelScope.Self)]
[TestFixture]
public class SecondTest : PageTest
{
    [Test]
    public async Task UploadAndResetFile()
    {
        await Page.GotoAsync("https://formy-project.herokuapp.com/");

        var fileUploadElement = Page.GetByRole(AriaRole.Link, new() { Name = "File Upload" });
        await fileUploadElement.ClickAsync();

        await Task.Delay(1000);

        await Page.SetInputFilesAsync(".input-ghost", "TestFile.txt");

        await Task.Delay(1000);

        var resetButton = Page.GetByRole(AriaRole.Button, new() { Name = "Reset" });
        await resetButton.ClickAsync();

        await Task.Delay(3000);
    }
}