﻿using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;

namespace MiTTPPLucic
{
    [Parallelizable(ParallelScope.Self)]
    [TestFixture]
    public class FifthTest : PageTest
    {
        [Test]
        public async Task DeviceEmulation()
        {
            var browser = await Playwright.Webkit.LaunchAsync();

            var context = await browser.NewContextAsync(new()
            {
                HasTouch = true,
                ViewportSize = new ViewportSize { Width = 320, Height = 658 },
                IsMobile = true,
                DeviceScaleFactor = 4,
                UserAgent = "Mozilla/5.0 (Linux; Android 8.0.0; SM-G965U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.6167.16 Mobile Safari/537.36"
            });

            var page = await context.NewPageAsync();

            await page.GotoAsync("https://formy-project.herokuapp.com");

            var buttonLink = page.GetByRole(AriaRole.Link, new() { Name = "Buttons" });
            if (buttonLink != null)
            {
                var box = await buttonLink.BoundingBoxAsync();
                if (box != null)
                {
                    var centerX = box.X + box.Width / 2;
                    var centerY = box.Y + box.Height / 2;

                    await page.Touchscreen.TapAsync(centerX, centerY);

                    await page.WaitForTimeoutAsync(2000);
                }
            }

            await browser.CloseAsync();
        }
    }
}
