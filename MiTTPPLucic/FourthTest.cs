﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Playwright;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;

namespace MiTTPPLucic;

[Parallelizable(ParallelScope.Self)]
[TestFixture]
public class FourthTest : PageTest
{
    [Test]
    public async Task DragDropAndScreenshot()
    {
        await Page.GotoAsync("https://formy-project.herokuapp.com/");

        var fileUploadElement = Page.GetByRole(AriaRole.Link, new() { Name = "Drag and Drop" });
        await fileUploadElement.ClickAsync();

        await Task.Delay(3000);

        await Page.Locator("#image").DragToAsync(Page.Locator("#box"));

        await Task.Delay(3000);

        await Page.Locator("#image").ScreenshotAsync(new() { Path = "screenshot.png" });

        await Task.Delay(1000);
    }
}