
# Metode i tehnike testiranja programske podrške - Projekt






## Sadržaj

- Opis projekta
- Korištene tehnologije i aplikacije
- Upute za pokretanje i instalaciju projekta
- Testni slučajevi
- Playwright inspector


## 1. Opis projekta

Ovaj projekt fokusiran je na primjenu metoda i tehnika testiranja programske podrške kroz korištenje NUnit i Playwright .NET okružja. NUnit je popularan okvir za testiranje u okružju NET-a, dok Playwright pruža moćan set alata za automatizaciju testiranja web aplikacija. Osobno, kroz projekt se prvi puta upoznajem sa Playwright .NET okružjem, te bi se i novi testeri kroz ovaj projekt i testne primjere trebali upoznati sa osnovama Playwright tehnologije. 

## 2. Korištene tehnologije i aplikacije

### 2.1. Git Fork za Verzioniranje

U ovom projektu koristio sam Fork kao alat za verzioniranje koda. Fork pruža jednostavno i intuitivno korisničko sučelje koje olakšava rad s Git repozitorijima. Ova aplikacija omogućava kloniranje, commitanje, pregledavanje promjena te upravljanje granama, sve kroz vizualno sučelje. 

![App Screenshot](https://i.postimg.cc/QdpzTBx9/Git.png)

Verzioniranje u projektu postignuto je na sljedeći način: Razvoj svakog testa je tekao na zasebnom branchu, kako bi se samo testna verzija mogla podići na develop, odnosno mergati, dok istestirana i konačna verzija se pusha na main branch. Na taj način, postignuta je pravilna struktura razvoja projekta, gdje se može razvijati više testova odjednom bez da jedan utječe na drugi. Kroz .gitignore file onemogućeno je generiranje nepotrebnih dokumenata te se tako izbjegava mogućnost pushanja zastarjelih verzija.

![AppScreenshot](https://i.postimg.cc/8z4nk2zx/Run-Settings.png)

Na fotografiji se nalazi dokument "RunSettings.xml" u kojima se mogu defenirati browseri u kojima će se pokretati testovi. Također, za svaki browser se mogu definirati i postavke pokretanja, pa tako ovdje, <Headless>false</Headless> predstavlja da će se otvarati zaseban browser prilikom pokretanja testa kroz RunSettings file. U suprotnom, test bi se također izvršio kroz ovaj browser, no on ne bi bio pokrenut.

### 2.2. Playwright

Playwright .NET je snažan alat za automatizaciju web aplikacija koji pruža API za kontrolu preglednika (Chromium, Firefox, WebKit) kroz .NET. Playwright olakšava manipulaciju web stranicama, izvođenje akcija kao što su klikovi, popunjavanje obrazaca, učitavanje dokumenata, odabir datuma i slično. Korištenjem Playwrighta zajedno s NUnit-om, osigurao sam stabilno i pouzdano izvođenje testova.

### 2.3. Visual Studio 2022

Za razvoj projekta koristio sam Visual Studio 2022, razvojno okruženje (IDE) koje pruža napredne alate za razvoj .NET aplikacija. Visual Studio 2022 omogućava jednostavno pisanje, testiranje i održavanje koda, podržavajući integraciju s NUnit testovima te pružajući napredne značajke poput debugiranja, refaktoriranja i praćenja izvedbe.


## 3. Upute za pokretanje i instalaciju projekta

- Potrebno je imati instaliran Visual Studio 2022 te verziju .NET Core-a najmanje 7.0.
- Nakon otvaranja solutiona, potrebno je buildati projekti prilikom čega će se instalirati potrebni NuGet paketi poput Playwright.NUnit
- Potrebno je instalirati novu verziju Powershella za pokretanje testova iz terminala
- Pomoću dotnet test pokrenuti testove

    


## 4. Testni slučajevi

### 4.1. Struktura projekta

![App Screenshot](https://i.postimg.cc/ZYx0VH7V/Struktura.png)

Projekt se sastoji od 5 testova, te je svaki test predstavljen klasom, kako bi se testovi mogli koristiti i u drugim klasama. Uz to, odvajanje testova u zasebne klase omogućuje pokretanje testova pojedinačno kroz terminal, te kroz odvojene browsere. Neke vrste testova se mogu pokretati samo u određenim browserima, te na ovaj način osiguravamo pravilno pokretanje testa.

### 4.2. Prvi testni slučaj - automatizirano ispunjavanje web forme

![App Screnshot](https://i.postimg.cc/KzrL7235/Test1.png)

Prvi testni slučaj odnosi se na automatizirano ispunjavanje web forme pomoću Playwright.NUnit metoda. Najprije pomoću Page.GotoAsync metode odlazimo na željenu web stranicu pomoću urla (string parametar). Postoje različiti načini kako dohvatiti neku komponentu web stranice, npr. GetByPlaceHolder koji pronalazi komponentu pomoću placeholdera unutar html komponente. FillAsync() metoda ispunjava polje sa stringom određenim u parametru. Sve metode su asinkrone, odnosno koristimo await kako bi spriječili izvršavanje metoda koje nisu ispunjene. Page.Locator je metoda pomoću koje dolazimo komponente na načine određene u parametru, npr "xpath=//..." dolazi do komponente pomoću xpatha. Nakon što smo potvrdili formu, okristimo Expect.ToBeVisibleAsync() metodu kako bi potvrdili da je forma ispunjena točno te kao parametre predajemo ono što očekujemo nakon potvrđivanja forme.

Testne slučajeve možemo pokrenuti na nekoliko načina:
```bash
  dotnet test
```
Ova naredba pokreće sve testove, odnosno klase koje imaju atribut [Test]
```bash
  dotnet test --filter "FirstTest"
```
Ova naredba pokreće samo filtrirani test po imenu klase

```bash
  dotnet test --filter "FirstTest" --settings:RunSettings.xml
```
Kroz ovu naredbu pokrećemo filtrirani test prema postavkama definiranim u RunSettings.xml.

![App Screenshot](https://i.postimg.cc/nh40R6mC/Test-Explorer.png)

Testove možemo pokretati i kroz Test Explorer u Visual Studiu. Možemo pokrenuti sve testove odjednom, no možemo i dodati u "playlistu" testove koje želimo izvršiti te redoslijedom kojim želimo.

### 4.3. Drugi testni slučaj - upload dokumenta

![App Screenshot](https://i.postimg.cc/RCsvbYmC/Test2.png)

Drugi testni primjer vezan je uz automatiziranjo uploadanje filea te reset, odnosno brisanje uploadanog filea.

Do komponente za prijelaz stranice dolazimo pomoću metode GetByRole() te AriaRole klasom biramo oznaku za link, te do njega dolazimo uz ono tekst koji piše, odnosno "File Upload".

Pomoću metode SetInputFilesAsync() prvi parametar predstavlja klasu komponente za upload file-a, a drugi parametar je ime file-a koji ćemo uploadati. TestFile.txt se nalazi u istom direktoriju kao i klasa te nije potrebno stavljati putanju. 

Nakon uploadanja file-a, dohvaćamo komponentu po imenu te pritiskom učitani file se briše.

![App Screenshot](https://i.postimg.cc/7hz8hwsq/File-Upload.png)

### 4.4. Treći testni slučaj - test performansi

Treći testni slučak vezan je uz testiranje performansi web stranice. PLaywright nudi različite metode, poput određivanja lokacije pomoću koordinata kao simulaciju slanja zahtjeva sa različitih geografskih lokacija. Uz to, možemo određivati veličinu zaslona, ili koristiti predefinirane zaslone za neke mobilne uređaje.

![App Screenshot](https://i.postimg.cc/k5Z7g1JF/Third-Test.png)

Pomoću metode SetGeolocationAsync() te parametara za zemljopisnu širinu i dužinu simuliramo geolokaciju sa koje dolaze zahtjevi. Koristimo window.performance API za mjerenje brzine učitavanja web stranice pod različitim parametrima, te dobivene rezultate pomoću Console.WriteLine zapisujemo u konzolu. Primjetimo da se podaci dobivaju u JSON obliku

```bash
Vrijeme potrebno za pokretanje: 0
Trajanje: 9,364800000000745 sekundi
Mjerenja performansi: {"connectStart":1705431421359,"navigationStart":1705431418987,"secureConnectionStart":1705431421406,"fetchStart":1705431418988,"domContentLoadedEventStart":1705431428338,"responseStart":1705431423249,"domInteractive":1705431427991,"domainLookupEnd":1705431421359,"responseEnd":1705431427983,"redirectStart":0,"requestStart":1705431421480,"unloadEventEnd":0,"unloadEventStart":0,"domLoading":1705431423261,"domComplete":1705431428339,"domainLookupStart":1705431421359,"loadEventStart":1705431428339,"domContentLoadedEventEnd":1705431428338,"loadEventEnd":1705431428341,"redirectEnd":0,"connectEnd":1705431421479}
```

Rezultat iz terminala prikazuje izmjerene performanse pri pokretanju web stranice  određenim uvjetima. Mjerenja performansi izražena su u milisekundama. Za primjer, performanse smo mogli usporiti promjenom geolokacije zahtjeva.

### 4.5. Četvrti testni slučaj - Drag and Drop i Screenshot

Playwright nudi mogućnosti snimanja zaslona, te prenošenja komponenti na druge lokacije unutar web stranice. 

![App Screenshot](https://i.postimg.cc/0jht6S64/Fourth-Test.png)

Pomoću Locator("#image") odnosno id-a komponente dolazimo do komponente koju želimo odvući na drugu lokaciju. DragToAsync() je asinkrona metoda koja kao parametar prima komponentu na koju prenosimo, te također do nje dolazimo preko id-a. 

Za snimanje zaslona koristimo ScreenshotAsync te definiramo putanju gdje želimo da se snimka zaslona spremi. Primjer snimke zaslona nalazi se ispod. Primjetimo da je snimljen zaslon samo one komponente koju smo odredili (#image).

![App Screenshot](https://i.postimg.cc/WzVGx7kT/screenshot.png)

### 4.6. Peti testni slučaj - simulacija zahtjeva sa zaslona mobitela

Kao što je već spomenuto, Playwright daje mogućnost simuliranja zahtjeva i korištenja toucha mobilnih uređaja. Potrebno je definirati parametre poput veličine zaslona i ostalih postavki koji će se primijeniti na test.

![App Screenshot](https://i.postimg.cc/8zf7B67H/Test5.png)

Pomoću definiranja novog contexta na browser, definiramo postavke poput HasTouch i IsMobile što znači da je touch omogućen i potrebno je simulirati zahtjev s mobilnog uređaja. ViewportSize parametar određuje veličinu zaslona, odnosno browsera koji će se otvoriti prilikom pokretanja testa. 

Test je jednostavan, potrebno je simulacijom toucha pritisnuti komponentu buttona. Najprije je potrebno definirati box, odnosno prostor oko željene komponente. Definiramo sredinu prostora, te pomocu metode TapAsync koristeći koordinate. 

## 5. Playwright inspector

Jedan vrlo koristan alat koju Playwright nudi naziva se Playwright inspector. To je alat korišten za debuggiranje testova, odnosno pomoću njega, ukoliko nismo sigurni kako doći do neke komponente, možemo to automatski. Pritisnemo na komponentu, a playwright inspector napiše kod kako doči do nje, odnosno na taj način možemo snimiti test te ga lakše primijeniti u kod. 

Za pokretanje playwright inspectora koristimo sljedeću naredbu:

```bash
  $env:PWDEBUG=1
  dotnet test
```
Prilikom pokretanja testa, pokrenuti će se dva prozora, jedan webkit browser koji će pokrenuti naš test, a drugi prozor će biti playwright inspector pomoću kojeg možemo debuggirati test korak po korak. 

![App Screenshot](https://i.postimg.cc/c1bW05JD/Playwright-Inspector.png)

Na fotografiji je prikazano sučelje Playwright inspectora, gdje možemo pronaći akcije poput snimanja testova, debuggiranja liniju po liniju.

![App Screenshot](https://i.postimg.cc/2SwrRjLW/Record.png)

Ukoliko na playwright inspectoru upalimo record, prijelazom preko bilo koje komponente na browseru automatski će se generirat kod kako doći do te komponente. Navedeno je prikazano fotografijom iznad.

